﻿using System;
using System.Linq;
using LibBaml;
using LibBaml.b4029;
using LibBaml.b4029.Records;

namespace ParseBaml
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = args.First(s => s.StartsWith("-file="));

            IBamlReader bRead = new BamlReader(file.Substring(6));

            var elements = bRead.Read();

            foreach(var elementObj in elements)
            {
                var element = elementObj as BamlRecord;
                Console.WriteLine("{1} ({0}b)", element.RecordSize, element.RecordType);
                Console.WriteLine("\t{0}", outputRecordData(element));
                Console.WriteLine();
            }
        }

        private static string outputRecordData(BamlRecord br)
        {
            switch (br.RecordType)
            {
                case BamlRecordType.AssemblyInfo:
                    BamlAssemblyInfoRecord bair = br as BamlAssemblyInfoRecord;
                    return string.Format("AssemblyId: {0}, AssemblyName: {1}", bair.AssemblyId, bair.AssemblyName);

                case BamlRecordType.TypeInfo:
                    BamlTypeInfoRecord btir = br as BamlTypeInfoRecord;
                    return string.Format("AssemblyId: {0}, TypeId: {1}, TypeFullName: {2}", btir.AssemblyId, btir.TypeId, btir.TypeFullName);

                case BamlRecordType.AttributeInfo:
                    BamlAttributeInfoRecord batir = br as BamlAttributeInfoRecord;
                    return string.Format("AttributeId: {0}, OwnerId: {1}, Name: {2}", batir.AttributeId, batir.OwnerId, batir.Name);

                case BamlRecordType.StartDocument:
                    BamlStartDocumentRecord bsdr = br as BamlStartDocumentRecord;
                    return string.Format("RootElement: {0}, LoadAsync: {1}, MaxAsyncRecords: {2}", bsdr.RootElement, bsdr.LoadAsync, bsdr.MaxAsyncRecords);

                case BamlRecordType.Element:
                    BamlElementRecord ber = br as BamlElementRecord;
                    return string.Format("Depth: {0}, ParentOffset: {1}, RightSiblingOffset: {2}, LeftSiblingCount: {3}, Id: {4}, ChildNodes: {5}, ElementNodes: {6}, FirstChildOffset: {7}",
                        ber.Depth, ber.ParentOffset, ber.RightSiblingOffset, ber.LeftSiblingCount, ber.Id, ber.ChildNodes, ber.ElementNodes, ber.FirstChildOffset);

                case BamlRecordType.ParseLiteralContent:
                    BamlLiteralContentRecord blcr = br as BamlLiteralContentRecord;
                    return string.Format("Depth: {0}, ParentOffset: {1}, RightSiblingOffset: {2}, LeftSiblingCount: {3}, Value: {4}, LineNumber: {5}, LinePosition: {6}",
                        blcr.Depth, blcr.ParentOffset, blcr.RightSiblingOffset, blcr.LeftSiblingCount, blcr.Value, blcr.LineNumber, blcr.LinePosition);

                case BamlRecordType.XmlnsProperty:
                    BamlXmlnsPropertyRecord bxpr = br as BamlXmlnsPropertyRecord;
                    return string.Format("Prefix: {0}, Value: {1}", bxpr.Prefix, bxpr.Value);

                case BamlRecordType.DynamicProperty:
                case BamlRecordType.DynamicPropertyCustom:
                    BamlDynamicPropertyRecord bdpr = br as BamlDynamicPropertyRecord;
                    return string.Format("AttributeId: {0}, Value: {1}, Complex: {2}", bdpr.AttributeId, bdpr.Value, bdpr.Complex);

                case BamlRecordType.DynamicEvent:
                    BamlDynamicEventRecord bder = br as BamlDynamicEventRecord;
                    return string.Format("AttributeId: {0}, Value: {1}", bder.AttributeId, bder.Value);

                case BamlRecordType.GenericAttribute:
                    BamlGenericAttributeRecord bgar = br as BamlGenericAttributeRecord;
                    return string.Format("NamespaceUri: {0}, LocalName: {1}, Value: {2}", bgar.NamespaceUri, bgar.LocalName, bgar.Value);

                case BamlRecordType.Text:
                    BamlTextRecord btr = br as BamlTextRecord;
                    return string.Format("Value: {0}", btr.Value);

                case BamlRecordType.ComplexDynamicProperty:
                    BamlComplexDynamicPropertyRecord bcdpr = br as BamlComplexDynamicPropertyRecord;
                    return string.Format("AttributeId: {0}", bcdpr.AttributeId);

                case BamlRecordType.ClrObject:
                    BamlClrObjectRecord bcor = br as BamlClrObjectRecord;
                    return string.Format("Depth: {0}, ParentOffset: {1}, RightSiblingOffset: {2}, LeftSiblingCount: {3}, Id: {4}",
                        bcor.Depth, bcor.ParentOffset, bcor.RightSiblingOffset, bcor.LeftSiblingCount, bcor.Id);

                case BamlRecordType.ClrProperty:
                    BamlClrPropertyRecord bcpr = br as BamlClrPropertyRecord;
                    return string.Format("Name: {0}, Value: {1}, FieldTypeId: {2}", bcpr.Name, bcpr.Value, bcpr.FieldTypeId);

                case BamlRecordType.ClrArrayProperty:
                    BamlClrArrayPropertyRecord bcapr = br as BamlClrArrayPropertyRecord;
                    return string.Format("Name: {0}", bcapr.Name);

                case BamlRecordType.ClrComplexProperty:
                    BamlClrComplexPropertyRecord bccpr = br as BamlClrComplexPropertyRecord;
                    return string.Format("Name: {0}", bccpr.Name);

                case BamlRecordType.IncludeTag:
                    BamlIncludeTagRecord bitr = br as BamlIncludeTagRecord;
                    return string.Format("Value: {0}", bitr.Value);
            }

            return "";
        }
    }
}
