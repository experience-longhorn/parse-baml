﻿using LibBaml.b4029.Records;
using System;
using System.Collections.Generic;
using System.IO;

namespace LibBaml.b4029
{
    public partial class BamlReader : IBamlReader
    {
        private BamlRecord[] readCache = new BamlRecord[25];

        public BamlReader(string path)
            : base(path)
        {
        }

        public override IBamlRecord[] Read()
        {
            List<IBamlRecord> items = new List<IBamlRecord>();

            IBamlRecord b;

            while ((b = getNextRecord()) != null)
            {
                items.Add(b);
            }

            return items.ToArray();
        }

        private BamlRecord getNextRecord()
        {
            long position = bamlStream.Position;
            long remaining = bamlStream.Length - position;

            if(remaining < 8L)
            {
                // no records remaining
                return null;
            }

            long recordSize = binReader.ReadInt64();

            if(recordSize <= 0L)
            {
                // err, negative/zero record size? gtfo
                bamlStream.Seek(position, SeekOrigin.Begin);
                return null;
            }

            if(recordSize > remaining)
            {
                // record size would take us beyond the end of the file?
                bamlStream.Seek(position, SeekOrigin.Begin);
                return null;
            }

            BamlRecord nextRecord = readNextRecord(position, recordSize);
            return nextRecord;
        }

        private BamlRecord readNextRecord(long position, long recordSize)
        {
            BamlRecord record;
            BamlRecordType recordType = (BamlRecordType)binReader.ReadInt16();

            switch(recordType)
            {
                case BamlRecordType.AssemblyInfo:
                    record = new BamlAssemblyInfoRecord();
                    break;
                case BamlRecordType.TypeInfo:
                    record = new BamlTypeInfoRecord();
                    break;
                case BamlRecordType.AttributeInfo:
                    record = new BamlAttributeInfoRecord();
                    break;
                default:
                    record = readCache[(int)recordType];
                    if(record == null)
                    {
                        record = readCache[(int)recordType] = allocateRecordType(recordType);
                    }
                    break;
            }

            if(record != null)
            {
                record.RecordSize = recordSize;
                record.FilePosition = position;
                record.LoadRecordData(binReader);
            }

            return record;
        }

        private BamlRecord allocateRecordType(BamlRecordType type)
        {
            switch(type)
            {
                case BamlRecordType.StartDocument:
                    return new BamlStartDocumentRecord();

                case BamlRecordType.EndDocument:
                    return new BamlEndDocumentRecord();

                case BamlRecordType.Element:
                    return new BamlElementRecord();

                case BamlRecordType.EndElement:
                    return new BamlEndElementRecord();

                case BamlRecordType.ParseLiteralContent:
                    return new BamlLiteralContentRecord();

                case BamlRecordType.XmlnsProperty:
                    return new BamlXmlnsPropertyRecord();

                case BamlRecordType.DynamicProperty:
                    return new BamlDynamicPropertyRecord();

                case BamlRecordType.DynamicEvent:
                    return new BamlDynamicEventRecord();

                case BamlRecordType.GenericAttribute:
                    return new BamlGenericAttributeRecord();

                case BamlRecordType.Text:
                    return new BamlTextRecord();

                case BamlRecordType.AssemblyInfo:
                case BamlRecordType.TypeInfo:
                case BamlRecordType.AttributeInfo:
                    throw new Exception("Attempted to allocate uncacheable records");

                case BamlRecordType.ComplexDynamicProperty:
                    return new BamlComplexDynamicPropertyRecord();

                case BamlRecordType.EndComplexDynamicProperty:
                    return new BamlEndComplexDynamicPropertyRecord();

                case BamlRecordType.ClrObject:
                    return new BamlClrObjectRecord();

                case BamlRecordType.EndClrObject:
                    return new BamlEndClrObjectRecord();

                case BamlRecordType.ClrProperty:
                    return new BamlClrPropertyRecord();

                case BamlRecordType.ClrArrayProperty:
                    return new BamlClrArrayPropertyRecord();

                case BamlRecordType.EndClrArrayProperty:
                    return new BamlEndClrArrayPropertyRecord();

                case BamlRecordType.ClrComplexProperty:
                    return new BamlClrComplexPropertyRecord();

                case BamlRecordType.EndClrComplexProperty:
                    return new BamlEndClrComplexPropertyRecord();

                case BamlRecordType.IncludeTag:
                    return new BamlIncludeTagRecord();

                case BamlRecordType.DynamicPropertyCustom:
                    return new BamlDynamicPropertyCustomRecord();
            }

            return null;
        }
    }
}
