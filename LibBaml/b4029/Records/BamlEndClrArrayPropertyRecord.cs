﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlEndClrArrayPropertyRecord : BamlRecord
    {
        public BamlEndClrArrayPropertyRecord()
        {
            RecordType = BamlRecordType.EndClrArrayProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
