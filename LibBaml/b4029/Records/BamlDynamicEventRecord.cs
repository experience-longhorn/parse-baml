﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlDynamicEventRecord : BamlRecord
    {
        public short AttributeId { get; set; }
        public string Value { get; set; }

        public BamlDynamicEventRecord()
        {
            RecordType = BamlRecordType.DynamicEvent;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            AttributeId = reader.ReadInt16();
            Value = reader.ReadString();
        }
    }
}
