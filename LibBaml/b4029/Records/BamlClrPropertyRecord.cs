﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlClrPropertyRecord : BamlRecord
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public short FieldTypeId { get; set; }

        public BamlClrPropertyRecord()
        {
            RecordType = BamlRecordType.ClrProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            Name = reader.ReadString();
            Value = reader.ReadString();
            FieldTypeId = reader.ReadInt16();
        }
    }
}
