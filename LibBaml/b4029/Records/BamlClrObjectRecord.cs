﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlClrObjectRecord : BamlNodeRecord
    {
        public short Id { get; set; }

        public BamlClrObjectRecord()
        {
            RecordType = BamlRecordType.ClrObject;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            base.LoadRecordData(reader);

            Id = reader.ReadInt16();
        }
    }
}
