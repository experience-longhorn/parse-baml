﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlTypeInfoRecord : BamlRecord
    {
        public short AssemblyId { get; set; }
        public short TypeId { get; set; }
        public string TypeFullName { get; set; }

        public BamlTypeInfoRecord()
        {
            RecordType = BamlRecordType.TypeInfo;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            TypeId = reader.ReadInt16();
            AssemblyId = reader.ReadInt16();
            TypeFullName = reader.ReadString();
        }
    }
}
