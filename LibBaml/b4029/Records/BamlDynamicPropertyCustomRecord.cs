﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlDynamicPropertyCustomRecord : BamlDynamicPropertyRecord
    {
        public BamlDynamicPropertyCustomRecord()
        {
            RecordType = BamlRecordType.DynamicPropertyCustom;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            AttributeId = reader.ReadInt16();

            switch (RecordSize)
            {
                case 14:
                    //guessing short enum
                    Value = reader.ReadInt16().ToString();
                    break;
                case 16:
                    //guessing int enum
                    Value = reader.ReadInt32().ToString();
                    break;
            }

            reader.BaseStream.Seek(FilePosition + RecordSize, SeekOrigin.Begin);
        }
    }
}
