﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlXmlnsPropertyRecord : BamlRecord
    {
        public string Prefix { get; set; }
        public string Value { get; set; }

        public BamlXmlnsPropertyRecord()
        {
            RecordType = BamlRecordType.XmlnsProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            Prefix = reader.ReadString();
            Value = reader.ReadString();
        }
    }
}
