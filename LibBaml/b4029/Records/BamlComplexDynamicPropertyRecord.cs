﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlComplexDynamicPropertyRecord : BamlRecord
    {
        public short AttributeId { get; set; }

        public BamlComplexDynamicPropertyRecord()
        {
            RecordType = BamlRecordType.ComplexDynamicProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            AttributeId = reader.ReadInt16();
        }
    }
}
