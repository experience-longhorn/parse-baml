﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlAssemblyInfoRecord : BamlRecord
    {
        public short AssemblyId { get; set; }
        public string AssemblyName { get; set; }

        public BamlAssemblyInfoRecord()
        {
            RecordType = BamlRecordType.AssemblyInfo;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            AssemblyId = reader.ReadInt16();
            AssemblyName = reader.ReadString();
        }
    }
}
