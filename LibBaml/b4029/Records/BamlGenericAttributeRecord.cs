﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlGenericAttributeRecord : BamlRecord
    {
        public string NamespaceUri { get; set; }
        public string LocalName { get; set; }
        public string Value { get; set; }

        public BamlGenericAttributeRecord()
        {
            RecordType = BamlRecordType.GenericAttribute;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            NamespaceUri = reader.ReadString();
            LocalName = reader.ReadString();
            Value = reader.ReadString();
        }
    }
}
