﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlTextRecord : BamlNodeRecord
    {
        public string Value { get; set; }

        public BamlTextRecord()
        {
            RecordType = BamlRecordType.Text;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            base.LoadRecordData(reader);

            Value = reader.ReadString();
        }
    }
}
