﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlLiteralContentRecord : BamlNodeRecord
    {
        public string Value { get; set; }
        public int LineNumber { get; set; }
        public int LinePosition { get; set; }

        public BamlLiteralContentRecord()
        {
            RecordType = BamlRecordType.ParseLiteralContent;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            base.LoadRecordData(reader);

            Value = reader.ReadString();
            LineNumber = reader.ReadInt32();
            LinePosition = reader.ReadInt32();
        }
    }
}
