﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlAttributeInfoRecord : BamlRecord
    {
        public short AttributeId { get; set; }
        public short OwnerId { get; set; }
        public string Name { get; set; }

        public BamlAttributeInfoRecord()
        {
            RecordType = BamlRecordType.AttributeInfo;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            AttributeId = reader.ReadInt16();
            OwnerId = reader.ReadInt16();
            Name = reader.ReadString();
        }
    }
}
