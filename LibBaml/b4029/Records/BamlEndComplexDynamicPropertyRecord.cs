﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlEndComplexDynamicPropertyRecord : BamlRecord
    {
        public BamlEndComplexDynamicPropertyRecord()
        {
            RecordType = BamlRecordType.EndComplexDynamicProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
