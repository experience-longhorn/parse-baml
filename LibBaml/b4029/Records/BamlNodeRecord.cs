﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlNodeRecord : BamlRecord
    {
        public short Depth { get; set; }
        public int ParentOffset { get; set; }
        public int RightSiblingOffset { get; set; }
        public short LeftSiblingCount { get; set; }

        public override void LoadRecordData(BinaryReader reader)
        {
            Depth = reader.ReadInt16();
            ParentOffset = reader.ReadInt32() + (int)FilePosition;
            RightSiblingOffset = reader.ReadInt32() + (int)FilePosition;
            LeftSiblingCount = reader.ReadInt16();
        }
    }
}
