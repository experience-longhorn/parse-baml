﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlClrComplexPropertyRecord : BamlRecord
    {
        public string Name { get; set; }

        public BamlClrComplexPropertyRecord()
        {
            RecordType = BamlRecordType.ClrComplexProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            Name = reader.ReadString();
        }
    }
}
