﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlEndClrComplexPropertyRecord : BamlRecord
    {
        public BamlEndClrComplexPropertyRecord()
        {
            RecordType = BamlRecordType.EndClrComplexProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
