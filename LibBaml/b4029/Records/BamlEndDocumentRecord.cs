﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlEndDocumentRecord : BamlRecord
    {
        public BamlEndDocumentRecord()
        {
            RecordType = BamlRecordType.EndDocument;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
