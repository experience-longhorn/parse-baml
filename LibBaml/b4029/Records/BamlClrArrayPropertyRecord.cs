﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlClrArrayPropertyRecord : BamlRecord
    {
        public string Name { get; set; }

        public BamlClrArrayPropertyRecord()
        {
            RecordType = BamlRecordType.ClrArrayProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            Name = reader.ReadString();
        }
    }
}
