﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlElementRecord : BamlNodeRecord
    {
        public short Id { get; set; }
        public short ChildNodes { get; set; }
        public short ElementNodes { get; set; }
        public int FirstChildOffset { get; set; }

        public BamlElementRecord()
        {
            RecordType = BamlRecordType.Element;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            base.LoadRecordData(reader);

            Id = reader.ReadInt16();
            ChildNodes = reader.ReadInt16();
            ElementNodes = reader.ReadInt16();
            FirstChildOffset = reader.ReadInt32() + (int)FilePosition;
        }
    }
}
