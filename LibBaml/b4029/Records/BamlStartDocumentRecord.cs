﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlStartDocumentRecord : BamlRecord
    {
        public int RootElement { get; set; }
        public bool LoadAsync { get; set; }
        public int MaxAsyncRecords { get; set; }

        public BamlStartDocumentRecord()
        {
            RecordType = BamlRecordType.StartDocument;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            RootElement = reader.ReadInt32() + (int)FilePosition;
            LoadAsync = reader.ReadBoolean();
            MaxAsyncRecords = reader.ReadInt32();
        }
    }
}
