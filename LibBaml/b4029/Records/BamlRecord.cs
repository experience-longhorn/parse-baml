﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlRecord : IBamlRecord
    {
        public BamlRecordType RecordType { get; set; }

        public virtual void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
