﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlDynamicPropertyRecord : BamlRecord
    {
        public short AttributeId { get; set; }
        public string Value { get; set; }
        public bool Complex { get; set; }

        public BamlDynamicPropertyRecord()
        {
            RecordType = BamlRecordType.DynamicProperty;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            AttributeId = reader.ReadInt16();
            Value = reader.ReadString();
            Complex = reader.ReadBoolean();
        }
    }
}
