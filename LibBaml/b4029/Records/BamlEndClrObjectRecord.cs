﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlEndClrObjectRecord : BamlRecord
    {
        public BamlEndClrObjectRecord()
        {
            RecordType = BamlRecordType.EndClrObject;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
