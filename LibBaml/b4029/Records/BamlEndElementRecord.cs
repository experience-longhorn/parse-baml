﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlEndElementRecord : BamlRecord
    {
        public BamlEndElementRecord()
        {
            RecordType = BamlRecordType.EndElement;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            return;
        }
    }
}
