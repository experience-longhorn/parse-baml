﻿using System.IO;

namespace LibBaml.b4029.Records
{
    public class BamlIncludeTagRecord : BamlRecord
    {
        public string Value { get; set; }

        public BamlIncludeTagRecord()
        {
            RecordType = BamlRecordType.IncludeTag;
        }

        public override void LoadRecordData(BinaryReader reader)
        {
            Value = reader.ReadString();
        }
    }
}
