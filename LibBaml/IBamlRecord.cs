﻿namespace LibBaml
{
    public abstract class IBamlRecord
    {
        public long FilePosition { get; set; }
        public long RecordSize { get; set; }
    }
}
