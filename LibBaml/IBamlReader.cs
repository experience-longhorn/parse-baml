﻿using System.IO;

namespace LibBaml
{
    public abstract class IBamlReader
    {
        protected BinaryReader binReader;
        protected Stream bamlStream;

        public IBamlReader(string path)
        {
            FileStream fStr = new FileStream(path, FileMode.Open);
            bamlStream = fStr;

            binReader = new BinaryReader(bamlStream);
        }

        public abstract IBamlRecord[] Read();
    }
}
